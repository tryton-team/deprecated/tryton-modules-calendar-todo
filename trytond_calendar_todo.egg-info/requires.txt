vobject >= 0.8.0
PyWebDAV >= 0.9.8
python-dateutil
pytz
python-sql
trytond_calendar >= 4.2, < 4.3
trytond_webdav >= 4.2, < 4.3
trytond >= 4.2, < 4.3
